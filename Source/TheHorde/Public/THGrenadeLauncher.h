// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "THWeapon.h"
#include "THGrenadeLauncher.generated.h"

class USkeletalMeshComponent;
class UDamageType;

/**
 * 
 */
UCLASS()
class THEHORDE_API ATHGrenadeLauncher : public ATHWeapon
{
	GENERATED_BODY()

protected:
	UPROPERTY(EditDefaultsOnly, Category = "GrenadeLauncher")
	TSubclassOf<AActor> ProjectileClass;
	
	virtual void Fire() override;

	UFUNCTION(Server, Reliable, WithValidation)
	void ServerFire();

};
