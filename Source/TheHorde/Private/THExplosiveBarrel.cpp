// Fill out your copyright notice in the Description page of Project Settings.

#include "THExplosiveBarrel.h"
#include "THHealthComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Particles/ParticleSystem.h"
#include "Kismet/GameplayStatics.h"
#include "PhysicsEngine/RadialForceComponent.h"
#include "Engine/World.h"
#include "Net/UnrealNetwork.h"


// Sets default values
ATHExplosiveBarrel::ATHExplosiveBarrel()
{
	MeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	MeshComp->SetSimulatePhysics(true);
	// Set to physics body to let the radial component affect us (eg. when a nearby barrel explodes)
	MeshComp->SetCollisionObjectType(ECC_PhysicsBody);
	RootComponent = MeshComp;

	HealthComp = CreateDefaultSubobject<UTHHealthComponent>(TEXT("HealthComponent"));
	HealthComp->OnHealthChanged.AddDynamic(this, &ATHExplosiveBarrel::OnHealthChanged);

	RadialForceComp = CreateDefaultSubobject<URadialForceComponent>(TEXT("RadialForceComponent"));
	RadialForceComp->SetupAttachment(RootComponent);
	RadialForceComp->Radius = 250;
	RadialForceComp->bImpulseVelChange = true;
	RadialForceComp->bAutoActivate = false; // Prevent component from ticking and only use FireImpulse() instead
	RadialForceComp->bIgnoreOwningActor = true; // Ignore self

	ExplosionImpulse = 400;

	SetReplicates(true);
	SetReplicateMovement(true);

}

void ATHExplosiveBarrel::OnHealthChanged(UTHHealthComponent* OwningHealthComp, float Health, float HealthDelta, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser)
{
	if (bExploded)
	{
		// Already exploded
		return;
	}

	if (Health <= 0.0f)
	{
		// Explode!
		bExploded = true;
		OnRep_Exploded();

		// Boost the barrel upwards
		FVector BoostIntensity = FVector::UpVector * ExplosionImpulse;
		MeshComp->AddImpulse(BoostIntensity, NAME_None, true);

		// Blast away nearby physics actors
		RadialForceComp->FireImpulse();

		// Apply radial damage
		UGameplayStatics::ApplyRadialDamage(
			this,
			ExplosionDamage,
			GetActorLocation(),
			RadialForceComp->Radius,
			UDamageType::StaticClass(),
			TArray<AActor*>() // damage all actors
		);
	}
	
}

void ATHExplosiveBarrel::OnRep_Exploded()
{
	// Play FX and change material to black
	if (ExplosionEffect && ExplodedMaterial)
	{
		// Play explosion effect
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ExplosionEffect, GetActorLocation());

		// Override material on mesh with blackened version
		MeshComp->SetMaterial(0, ExplodedMaterial);
	}
}

void ATHExplosiveBarrel::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ATHExplosiveBarrel, bExploded);
}

