# The Horde
Co-op third person shooter where the players face hordes of enemies. Features advanced AI, power-ups, weapons and networking using Unreal Engine 4.

![](TheHorde.mp4)
