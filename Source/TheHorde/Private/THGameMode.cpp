// Fill out your copyright notice in the Description page of Project Settings.

#include "THGameMode.h"
#include "THGameState.h"
#include "THPlayerState.h"
#include "TimerManager.h"
#include "Engine/World.h"
#include "Components/THHealthComponent.h"

ATHGameMode::ATHGameMode()
{
	GameStateClass = ATHGameState::StaticClass();
	PlayerStateClass = ATHPlayerState::StaticClass();

	TimeBetweenWaves = 2.0f;

	// Set tick to 1 second
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.TickInterval = 1.0f;
}

void ATHGameMode::StartPlay()
{
	Super::StartPlay();

	PrepareForNextWave();
}

void ATHGameMode::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	CheckWaveState();

	CheckAnyPlayerAlive();
}

void ATHGameMode::StartWave()
{
	WaveCount++;

	NumberOfBotsToSpawn = 2 * WaveCount;

	GetWorldTimerManager().SetTimer(TimerHandle_BotSpawn, this, &ATHGameMode::SpawnBotTimerElapsed, 1.0f, true, 0.0f);

	SetWaveState(EWaveState::WaveInProgress);
}

void ATHGameMode::PrepareForNextWave()
{
	GetWorldTimerManager().SetTimer(TimerHandle_NextWaveStart, this, &ATHGameMode::StartWave, TimeBetweenWaves, false);

	SetWaveState(EWaveState::WaitingToStart);

	RespawnDeadPlayers();
}

void ATHGameMode::CheckWaveState()
{
	bool bIsPreparingForWave = GetWorldTimerManager().IsTimerActive(TimerHandle_NextWaveStart);

	// Make sure wave is complete first
	if (NumberOfBotsToSpawn > 0 || bIsPreparingForWave)
	{
		return;
	}

	bool bIsAnyBotAlive = false;

	for (FConstPawnIterator It = GetWorld()->GetPawnIterator(); It; ++It)
	{
		APawn* TestPawn = It->Get();
		if (TestPawn == nullptr || TestPawn->IsPlayerControlled())
		{
			continue;
		}

		UTHHealthComponent* HealthComp = Cast<UTHHealthComponent>(TestPawn->GetComponentByClass(UTHHealthComponent::StaticClass()));
		if (HealthComp && HealthComp->GetHealth() > 0.0f)
		{
			bIsAnyBotAlive = true;
			break;
		}
	}

	if (!bIsAnyBotAlive)
	{
		SetWaveState(EWaveState::WaveComplete);

		PrepareForNextWave();
	}
}

void ATHGameMode::SetWaveState(EWaveState NewState)
{
	ATHGameState* GameState = GetGameState<ATHGameState>();
	if (ensureAlways(GameState))
	{
		GameState->SetWaveState(NewState);
	}
}

void ATHGameMode::CheckAnyPlayerAlive()
{
	for (FConstPlayerControllerIterator It = GetWorld()->GetPlayerControllerIterator(); It; ++It)
	{
		APlayerController* PlayerController = It->Get();
		if (PlayerController && PlayerController->GetPawn())
		{
			APawn* MyPawn = PlayerController->GetPawn();
			
			UTHHealthComponent* HealthComp = Cast<UTHHealthComponent>(MyPawn->GetComponentByClass(UTHHealthComponent::StaticClass()));
			if (ensure(HealthComp) && HealthComp->GetHealth() > 0.0f)
			{
				// A player is still alive
				return;
			}
		}
	}

	// No player alive
	GameOver();
}

void ATHGameMode::RespawnDeadPlayers()
{
	// Game mode is only run on the server so player pawn restart can be handled here
	for (FConstPlayerControllerIterator It = GetWorld()->GetPlayerControllerIterator(); It; ++It)
	{
		APlayerController* PlayerController = It->Get();
		if (PlayerController && PlayerController->GetPawn() == nullptr)
		{
			// Built in function
			RestartPlayer(PlayerController);
		}
	}
}

void ATHGameMode::SpawnBotTimerElapsed()
{
	// Handled in BP
	SpawnNewBot();

	NumberOfBotsToSpawn--;

	if (NumberOfBotsToSpawn <= 0)
	{
		EndWave();
	}
}

void ATHGameMode::EndWave()
{
	GetWorldTimerManager().ClearTimer(TimerHandle_BotSpawn);

	SetWaveState(EWaveState::WaitingToComplete);
}

void ATHGameMode::GameOver()
{
	EndWave();

	// TODO finish match and present game over to players

	SetWaveState(EWaveState::GameOver);

	UE_LOG(LogTemp, Log, TEXT("GAME OVER!"));
}
