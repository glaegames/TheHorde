// Fill out your copyright notice in the Description page of Project Settings.

#include "THGameState.h"
#include "Net/UnrealNetwork.h"

void ATHGameState::SetWaveState(EWaveState NewState)
{
	if (Role == ROLE_Authority)
	{
		EWaveState OldState = WaveState;
		
		WaveState = NewState;

		// Call on server
		OnRep_WaveState(OldState);
	}
}

void ATHGameState::OnRep_WaveState(EWaveState OldState)
{
	WaveStateChanged(WaveState, OldState);
}

void ATHGameState::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ATHGameState, WaveState);
}
