// Fill out your copyright notice in the Description page of Project Settings.

#include "THGrenadeLauncher.h"
#include "Engine/World.h"
#include "DrawDebugHelpers.h"
#include "Kismet/GameplayStatics.h"
#include "Components/SkeletalMeshComponent.h"
#include "Net/UnrealNetwork.h"

void ATHGrenadeLauncher::Fire()
{
	// Check if player role is client and delegate method call to server as well
	if (Role < ROLE_Authority)
	{
		ServerFire();
	}

	AActor* MyOwner = GetOwner();
	if (MyOwner && ProjectileClass)
	{
		FVector EyeLocation;
		FRotator EyeRotation;
		MyOwner->GetActorEyesViewPoint(EyeLocation, EyeRotation);

		FVector MuzzleLocation = MeshComp->GetSocketLocation(MuzzleSocketName);

		//Set Spawn Collision Handling Override
		FActorSpawnParameters ActorSpawnParams;
		ActorSpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

		// Spawn the projectile at the muzzle
		GetWorld()->SpawnActor<AActor>(ProjectileClass, MuzzleLocation, EyeRotation, ActorSpawnParams);
	}
}

/* Server implementation of fire method */
void ATHGrenadeLauncher::ServerFire_Implementation()
{
	Fire();
}

/* Method used for anti-cheat ie. validate the code */
bool ATHGrenadeLauncher::ServerFire_Validate()
{
	return true;
}
