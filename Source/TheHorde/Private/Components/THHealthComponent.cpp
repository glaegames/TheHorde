// Fill out your copyright notice in the Description page of Project Settings.

#include "THHealthComponent.h"
#include "THGameMode.h"
#include "Engine/World.h"
#include "GameFramework/Actor.h"
#include "Net/UnrealNetwork.h"


// Sets default values for this component's properties
UTHHealthComponent::UTHHealthComponent()
{
	DefaultHealth = 100;
	bIsDead = false;

	TeamNumber = 255;

	SetIsReplicated(true);
}


// Called when the game starts
void UTHHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	// Only hook if we are server
	if (GetOwnerRole() == ROLE_Authority)
	{
		AActor* MyOwner = GetOwner();
		if (MyOwner)
		{
			MyOwner->OnTakeAnyDamage.AddDynamic(this, &UTHHealthComponent::HandleTakeAnyDamage);
		}
	}

	Health = DefaultHealth;
}

float UTHHealthComponent::GetHealth() const
{
	return Health;
}

void UTHHealthComponent::HandleTakeAnyDamage(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser)
{
	if (Damage <= 0.0f || bIsDead)
	{
		return;
	}

	// Don't damage if friendly
	if (DamageCauser != DamagedActor && IsFriendly(DamagedActor, DamageCauser))
	{
		return;
	}

	// Update health clamped between 0 and default health value
	Health = FMath::Clamp(Health - Damage, 0.0f, DefaultHealth);

	// UE_LOG(LogTemp, Log, TEXT("Health changed: %s"), *FString::SanitizeFloat(Health));

	bIsDead = Health <= 0.0f;

	// UE_LOG(LogTemp, Log, TEXT("Dead = %s"), bIsDead ? TEXT("True") : TEXT("False"));

	OnHealthChanged.Broadcast(this, Health, Damage, DamageType, InstigatedBy, DamageCauser);

	if (bIsDead)
	{
		ATHGameMode* GameMode = Cast<ATHGameMode>(GetWorld()->GetAuthGameMode());
		if (GameMode)
		{
			GameMode->OnActorKilled.Broadcast(GetOwner(), DamageCauser, InstigatedBy);
		}
	}
}

void UTHHealthComponent::OnRep_Health(float OldHealth)
{
	float Damage = Health - OldHealth;

	OnHealthChanged.Broadcast(this, Health, Damage, nullptr, nullptr, nullptr);
}

void UTHHealthComponent::Heal(float HealAmount)
{
	if (HealAmount <= 0.0f || Health <= 0.0f)
	{
		return;
	}

	Health = FMath::Clamp(Health + HealAmount, 0.0f, DefaultHealth);

	UE_LOG(LogTemp, Log, TEXT("Health changed: %s (+%s)"), *FString::SanitizeFloat(Health), *FString::SanitizeFloat(HealAmount));

	OnHealthChanged.Broadcast(this, Health, -HealAmount, nullptr, nullptr, nullptr);
}

bool UTHHealthComponent::IsFriendly(AActor* ActorA, AActor* ActorB)
{
	if (ActorA == nullptr || ActorB == nullptr)
	{
		// Assume friendly
		return true;
	}

	UTHHealthComponent* HealthCompA = Cast<UTHHealthComponent>(ActorA->GetComponentByClass(UTHHealthComponent::StaticClass()));
	UTHHealthComponent* HealthCompB = Cast<UTHHealthComponent>(ActorB->GetComponentByClass(UTHHealthComponent::StaticClass()));

	if (HealthCompA == nullptr || HealthCompB == nullptr)
	{
		// Assume friendly
		return true;
	}

	return HealthCompA->TeamNumber == HealthCompB->TeamNumber;
}

void UTHHealthComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UTHHealthComponent, Health);
}
